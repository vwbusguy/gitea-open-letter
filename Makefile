default: ## Build the website
	./scripts/spellcheck.sh --check
	./scripts/prep.sh
	./scripts/zola.sh build

clean: ## Clean build assets
	./scripts/zola.sh clean

ci-deploy: ## Deploy from CI/CD. Only call from within CI
	@if [ "${CI}" != "woodpecker" ]; \
	then echo "Only call from within CI. Will re-write your local Git configuration. To override, set export CI=woodpecker"; \
		 exit 1; \
	fi
	git config --global user.email "${CI_COMMIT_AUTHOR_EMAIL}"
	git config --global user.name "${CI_COMMIT_AUTHOR}"
	./scripts/zola.sh deploy pages public "${CI_COMMIT_AUTHOR} <${CI_COMMIT_AUTHOR_EMAIL}>"
	./scripts/ci.sh --init "$$REPO_WRITE_DEPLOY_KEY"
	./scripts/ci.sh --deploy pages
	./scripts/ci.sh --clean

env: ## Download build dependencies and setup dev environment
	./scripts/zola.sh install

help: ## Prints help for targets with comments
	@cat $(MAKEFILE_LIST) | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

serve: ## Serve website during development
	./scripts/zola.sh zola -- serve
